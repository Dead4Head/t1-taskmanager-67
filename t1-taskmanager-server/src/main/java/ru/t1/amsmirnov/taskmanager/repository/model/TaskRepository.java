package ru.t1.amsmirnov.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.List;

public interface TaskRepository extends AbstractUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull final String projectId);

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);


}
