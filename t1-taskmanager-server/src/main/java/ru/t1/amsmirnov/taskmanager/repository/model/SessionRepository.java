package ru.t1.amsmirnov.taskmanager.repository.model;

import ru.t1.amsmirnov.taskmanager.model.Session;


public interface SessionRepository extends AbstractUserOwnedRepository<Session> {

}

