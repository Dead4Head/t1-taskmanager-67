package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.client.TaskClient;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Category(DBCategory.class)
public class TaskEndpointImplTest {

    private static final String BASE_URL = "http://localhost:8080/api/tasks";
    private static final String TEST_PREFIX = "DATABASE_TEST_";
    private static final int TEST_TASKS_COUNT = 3;
    private static final List<TaskWebDto> TASKS = new ArrayList<>();

    private final TaskClient client = TaskClient.client(BASE_URL);

    @Before
    public void initTest() {
        for (int i = 0; i < TEST_TASKS_COUNT; i++) {
            TaskWebDto task = new TaskWebDto(TEST_PREFIX + i);
            client.save(task);
            TASKS.add(task);
        }
    }

    @After
    public void clean() {
        TASKS.clear();
        client.deleteAll();
    }

    @Test
    public void testFindAll() {
        Collection<TaskWebDto> testFind = client.findAll();
        Assert.assertEquals(TASKS.size(), testFind.size());
    }

    @Test
    public void testFindById() {
        TaskWebDto task = client.findById(TASKS.get(0).getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), TASKS.get(0).getId());
    }

    @Test
    public void testDelete() {
        @NotNull final String id = TASKS.get(0).getId();
        client.delete(TASKS.get(0));
        Assert.assertNull(client.findById(id));
    }

    @Test
    public void testDeleteById() {
        @NotNull final String id = TASKS.get(0).getId();
        client.delete(id);
        Assert.assertNull(client.findById(id));
    }

    @Test
    public void testSave() {
        @NotNull final TaskWebDto task = new TaskWebDto(TEST_PREFIX + "TEST_SAVE");
        client.save(task);
        Assert.assertNotNull(task);
        String taskId = task.getId();
        TaskWebDto taskTest = client.findById(taskId);
        Assert.assertNotNull(taskTest);
        client.delete(task);
    }

}
