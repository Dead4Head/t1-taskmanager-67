package ru.t1.amsmirnov.taskmanager.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.t1.amsmirnov.taskmanager.api.ITaskEndpoint;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.service.dto.TaskDtoService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping(value = "/api/tasks", produces = "application/json")
@WebService(endpointInterface = "ru.t1.amsmirnov.taskmanager.api.ITaskEndpoint")
public class TaskEndpointImpl implements ITaskEndpoint {

    @Autowired
    private TaskDtoService taskService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<TaskWebDto> findAll() {
        try {
            return taskService.findAll();
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public TaskWebDto findById(
            @WebParam(name = "id", partName = "id") @PathVariable("id") final String id
    ) {
        try {
            return taskService.findOneById(id);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public TaskWebDto save(
            @WebParam(name = "task", partName = "task") @RequestBody final TaskWebDto task
    ) {
        try {
            return taskService.update(task);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public TaskWebDto delete(
            @WebParam(name = "task", partName = "task") @RequestBody final TaskWebDto task
    ) {
        try {
            return taskService.removeOne(task);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public TaskWebDto delete(
            @WebParam(name = "id", partName = "id") @PathVariable("id") final String id
    ) {
        try {
            return taskService.removeOneById(id);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        try {
            taskService.removeAll();
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

}
