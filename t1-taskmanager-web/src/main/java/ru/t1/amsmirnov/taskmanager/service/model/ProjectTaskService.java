package ru.t1.amsmirnov.taskmanager.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ProjectNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.TaskNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.ProjectIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.TaskIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.model.ProjectWeb;
import ru.t1.amsmirnov.taskmanager.model.TaskWeb;
import ru.t1.amsmirnov.taskmanager.repository.model.TaskRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectTaskService {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @NotNull
    @Autowired
    private ProjectService projectService;

    public ProjectTaskService() {
    }

    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existById(projectId)) throw new ProjectNotFoundException();
        final Optional<TaskWeb> optionalTask = taskRepository.findById(taskId);
        if (!optionalTask.isPresent()) throw new TaskNotFoundException();
        final ProjectWeb project = projectService.findOneById(projectId);
        final TaskWeb task = optionalTask.get();
        task.setProjectWeb(project);
        taskRepository.save(task);
    }

    @Transactional
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectService.existById(projectId)) throw new ProjectNotFoundException();
        final List<TaskWeb> tasks = taskRepository.findAllByProjectWebId(projectId);
        if (tasks == null) throw new ModelNotFoundException("TaskList");
        taskRepository.deleteAll(tasks);
        projectService.removeOneById(projectId);
    }

    @Transactional
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existById(projectId)) throw new ProjectNotFoundException();
        final Optional<TaskWeb> optionalTask = taskRepository.findById(taskId);
        if (!optionalTask.isPresent()) throw new TaskNotFoundException();
        final TaskWeb task = optionalTask.get();
        task.setProjectWeb(null);
        taskRepository.save(task);
    }

}
