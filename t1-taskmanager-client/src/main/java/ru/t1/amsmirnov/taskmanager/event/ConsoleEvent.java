package ru.t1.amsmirnov.taskmanager.event;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationEvent;

public class ConsoleEvent extends ApplicationEvent {

    @NotNull
    private final String name;


    public ConsoleEvent(@NotNull final Object source, @NotNull final String name) {
        super(source);
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
