package ru.t1.amsmirnov.taskmanager.dto.response.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class DataJsonSaveJaxbResponse extends AbstractResultResponse {

    public DataJsonSaveJaxbResponse() {
    }

    public DataJsonSaveJaxbResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
